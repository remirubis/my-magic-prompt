
# My magic prompt

Create bash program with multiple commands for learn bash programming


## Features

- For use this script user have to been logged with login and password
- We have manies shortcut for use commands
- User can change his password and view it

## Default access

Use following data for use this script
**Login**: user
**Password**: 1234

## List of all commands
| Command | Parameter |  Description | 
|--|--|--|
| about | / | What is this program? |
| age | / | What is your age, I tell you if your are a teenager or an adult! |
| cd | foldername | Navigate to folders in your computer |
| clear | / | Clear all history of terminal |
| help | / | List of commands |
| hour | / | Get current time |
| httpget | URL | Download index file from a website and change filename |
| ls | / | List all files and folders (hidden or not) in current folder |
| open | filename | Edit file with vim package |
| pass | / | View your password |
| passw | / | Change script access password with confirmation |
| profile | / | Who is the author of this script |
| pwd | / | Where are you? |
| quit | / | Leave the program |
| rm | filename | Remove file |
| rmd ou rmdir | foldername | Remove folder with password verification |
| rmdirwtf  | [foldername] | Remove folder |
| rps | / | Play with your friend Rock Paper Scissors |
| smtp | / | Send an email with subject and body to my email address |
| version ou --v ou vers | / | Version of this program |
| * | / | By default a message is sended if your command does not exist |

## Package

Package used in this script
- [mailutils](https://mailutils.org/)