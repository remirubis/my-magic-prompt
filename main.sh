#!/bin/bash
#Configure user data
root_log="user"
root_pass="1234"

#What is this shit ?
about() {
  echo "This program is a shortcut for multiple command usage."
  echo "If you want help use help command."
  echo "Private jock with command RPS try it ;)"
}

#What's your age ? - Test if it's okay for date this person
age() {
  re='^[0-9]+$'
  status=true
  while $status
  do
    read -p "Type your age: " age
    if [[ $age =~ $re ]]; then
      if [[ $((age)) -ge 18 ]]; then
        echo "Oh yeah you are an adult!"
      else
        echo -e "POLICE OPEN UP..."
        sleep 1
        echo -e "I am writing this message from my cell, you are a child :'("
      fi 
      status=false
    else
      echo "Please type a number greater than 0!"
    fi
  done
}

#Execution of command who user want
cmd() {
  while true
  do
    echo
    read -p "Type a command: " args

    tArgs=($args)
    cmd="${tArgs[0]}"
    unset tArgs[0]

    case $cmd in
      about ) about;;
      age ) age;;
      cd ) cd ${tArgs[1]};;
      clear ) clear && printf '\e[3J';;
      help ) display_help;;
      hour ) date +"%T";;
      httpget ) httpget ${tArgs[1]};;
      ls ) ls -la;;
      open ) vim ${tArgs[1]};;
      pass ) pass;;
      passw ) passw;;
      profile ) profile;;
      pwd ) pwd;;
      quit ) quit;;
      rm ) perso_rm ${tArgs[1]};;
      rmd | rmdir ) rmd ${tArgs[1]};;
      rmdirwtf ) rmdirwtf $tArgs;;
      rps ) rps;;
      smtp ) smtp;;
      version | vers | --v ) echo "Version 1.0.0";;
      * ) printf "Unknow command, please use \"help\" for more informations!";;
    esac
  done
}

#Help function for displaying all informations
display_help() {
  clear
  echo -e "\033[1mabout :\033[0m \033[3mWhat is this program?\033[0m"
  echo -e "\033[1mage :\033[0m \033[3mWhat is your age, I tell you if your are a teenager or an adult!\033[0m"
  echo -e "\033[1mcd :\033[0m \033[3mNavigate to folders in your computer\033[0m"
  echo -e "\033[1mclear :\033[0m \033[3mClear all history of terminal\033[0m"
  echo -e "\033[1mhelp :\033[0m \033[3mList of commands\033[0m"
  echo -e "\033[1mhour :\033[0m \033[3mGet current time\033[0m"
  echo -e "\033[1mhttpget :\033[0m \033[3mDownload index file from a website and change filename\033[0m"
  echo -e "\033[1mls :\033[0m \033[3mList all files and folders (hidden or not) in current folder\033[0m"
  echo -e "\033[1mopen :\033[0m \033[3mEdit file with vim package\033[0m"
  echo -e "\033[1mpass :\033[0m \033[3mView your password\033[0m"
  echo -e "\033[1mpassw :\033[0m \033[3mChange script access password with confirmation\033[0m"
  echo -e "\033[1mprofile :\033[0m \033[3mWho is the author of this script\033[0m"
  echo -e "\033[1mpwd :\033[0m \033[3mWhere are you?\033[0m"
  echo -e "\033[1mquit :\033[0m \033[3mLeave the program\033[0m"
  echo -e "\033[1mrm :\033[0m \033[3mRemove file\033[0m"
  echo -e "\033[1mrmd ou rmdir :\033[0m \033[3mRemove folder\033[0m"
  echo -e "\033[1mrmdirwtf :\033[0m \033[3mRemove folders with password verification\033[0m"
  echo -e "\033[1mrps :\033[0m \033[3mPlay with your friend Rock Paper Scissors\033[0m"
  echo -e "\033[1msmtp :\033[0m \033[3mSend an email with subject and body to my email address\033[0m"
  echo -e "\033[1mversion ou --v ou vers :\033[0m  \033[3mVersion of this program033[0m"
  echo -e "\033[1m* :\033[0m \033[3mBy default a message is sended if your command does not exist\033[0m"
  echo
}

#Download index.html file from a website
httpget() {
  if [[ ! -z "${tArgs[1]}" ]]; then
    read -p "Enter filename: " filename
    wget -O $filename "${tArgs[1]}"
  else
    echo "Missing URL: Please enter url like this http://www.remi-rubis.fr"
  fi
}

#View password
pass() {
  read -s -p "Please enter your password: " pass
  echo
  if [[ $pass == $root_pass ]]; then
    echo "Your password is $root_pass"
    sleep 3
    clear
    printf '\e[3J'
  else
    echo "Wrong password! Try again later..."
  fi
}

#Change the password
passw() {
  read -s -p "Please type your old password: " old
  echo
  if [[ $old == $root_pass ]]; then
    read -s -p "Please type your new password: " new
    echo
    read -s -p "Please type your new password again: " newVerif
    echo
    if [[ $new == $newVerif ]]; then
      root_pass=$new
      echo "Well done! New password have been set."
    else
      echo "Password don't match, please try again!"
    fi
  else
    echo "Wrong password! Try again later..."
  fi
}

#Remove file
perso_rm() {
  if [[ ! -z "${tArgs[1]}" ]] && [[ -f "${tArgs[1]}" ]]; then
    rm "./${tArgs[1]}"
    echo "File ${tArgs[1]} removed!"
  else
    echo "Please enter a filename, check if you are typing correctly..."
  fi
}

#Who write this program
profile() {
  echo "Firstname: Rémi"
  echo "Lastname: Rubis"
  echo "Age: 19"
  echo "Email: remi.rubis@yahoo.com"
}

#Leave the program - Please leave me alone...
quit() {
  echo "Bye Bye"
  exit 1
}

#Remove directory
rmd() {
  if [[ ! -z "${tArgs[1]}" ]] && [[ -d "${tArgs[1]}" ]]; then
    rm -r "./${tArgs[1]}"
    echo "Folder ${tArgs[1]} removed!"
  else
    echo "Please enter a folder name, check if you are typing correctly..."
  fi
}

#Remove one or more folders
rmdirwtf() {
  if [[ ! -z "${tArgs[1]}" ]]; then
    read -s -p "Please enter your password for doing this: " pass
    echo
    if [[ $pass == $root_pass ]]; then
      rm -r ${tArgs[@]}
      echo "Folder ${tArgs[@]} removed!"
    else
      echo "Wrong password! Try again later..."
    fi
  else
    echo "Please enter a folder name, check if you are typing correctly..."
  fi
}

#Play Rock, paper, Scissors
rps() {
  round=3

  read -p "Player 1: " p1
  read -p "Player 2: " p2

  isNb='^[0-3]+$'

  p1Score=0
  p2Score=0

  while [ $(($round)) -gt 0 ]
  do

    p1Win=0
    p2Win=0

    while true
    do
      echo "$p1 choose his attack:"
      echo "0 : Rock"
      echo "1 : Paper"
      echo "2 : Scissors"
      echo "3 : SUPERKITTY (DO NOT USE BE CAUTION IT'S DANGEROUS!!!!!!)"
      read -p "$p1 enter the number of your attack: " p1Attack
      echo

      clear
      if ! [[ $p1Attack =~ $isNb ]]; then
        echo "[ERROR] Please enter a choice in this list..."
        echo
      else
        echo "Next player..."
        echo
        break
      fi
    done

    while true
    do
      echo "$p2 choose his attack:"
      echo "0 : Rock"
      echo "1 : Paper"
      echo "2 : Scissors"
      echo "3 : SUPERKITTY (DO NOT USE BE CAUTION IT'S DANGEROUS!!!!!!)"
      read -p "$p2 enter the number of your attack: " p2Attack
      echo

      clear
      if ! [[ $(($p2Attack)) =~ $isNb ]]; then
        echo "[ERROR] Please enter a choice in this list..."
        echo
      else
        break
      fi
    done

    if [ $(($p1Attack)) -eq 0 ]; then
      [[ $(($p2Attack)) -eq 1 ]] && p2Win=1
      [[ $(($p2Attack)) -eq 2 ]] && p1Win=1
      [[ $(($p2Attack)) -eq 3 ]] && p2Win=1
    elif [ $(($p1Attack)) -eq 1 ]; then
      [[ $(($p2Attack)) -eq 0 ]] && p1Win=1
      [[ $(($p2Attack)) -eq 2 ]] && p2Win=1
      [[ $(($p2Attack)) -eq 3 ]] && p2Win=1
    elif [ $(($p1Attack)) -eq 2 ]; then
      [[ $(($p2Attack)) -eq 0 ]] && p2Win=1
      [[ $(($p2Attack)) -eq 1 ]] && p1Win=1
      [[ $(($p2Attack)) -eq 3 ]] && p2Win=1
    else
      [[ $(($p2Attack)) -ne 3 ]] && p1Win=1
    fi

    clear

    if [ $(($p1Win)) -eq 0 ] && [ $(($p2Win)) -eq 0 ]; then
      echo "It's a draw!"
      sleep 2
    elif [ $(($p1Win)) -eq 1 ]; then
      echo "$p1 win this round!"
      sleep 2
      ((p1Score++))
    else
      echo "$p2 win this round!"
      sleep 2
      ((p2Score++))
    fi

    ((round--))
  done

  if [ $p1Score -gt $p2Score ]; then
    winner=$p1
  else
    winner=$p2
  fi
  echo "WINNER IS $winner, CONGRATULATION!!!!!!!!"
  echo "Final score..."
  echo "$p1: $p1Score"
  echo "$p2: $p2Score"
}

#Send email to my google account
smtp() {
  if ! [ -x "$(command -v mailutils)" ]; then
    echo "Trying to download mailutils package..."
    brew install mailutils
  fi
  read -p "Enter a destinator: " destinator
  read -p "Enter a subject: " subject
  read -p "Enter a body: " body
  mail -s "$subject" $destinator <<< "$body"
  echo "Email have been send."
}


#Main function for login user and find command needed
main() {
  read -p "Login: " login
  read -s -p "Password: " pswd
  echo

  if [ "$login" != "$root_log" ] || [ "$pswd" != "$root_pass" ]; then
    printf "Login or password incorrect, please try again."
    exit
  fi

  cmd
}

#Launch this program <3
main
